       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BirthDateProgram.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 BirthDate.
           02 YearBirth.
              03 CenturyOB PIC 99.
              03 YearOB PIC 99.
           02 MonthOfBirth PIC 99.
           02 DayOfBirth PIC 99.
       PROCEDURE DIVISION .
           MOVE 20010413 TO BirthDate 
           DISPLAY "Month is = " MonthOfBirth 
           DISPLAY "Century of Birth is = " CenturyOB 
           DISPLAY "Year of Birth is = " YearBirth 
           DISPLAY DayOfBirth  "/" MonthOfBirth "/" YearBirth
           MOVE ZEROS TO YearBirth 
           DISPLAY "Birth date = " BirthDate .
       END PROGRAM BirthDateProgram.
